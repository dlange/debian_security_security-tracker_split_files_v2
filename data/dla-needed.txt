An LTS security update is needed for the following source packages.
When you add a new entry, please keep the list alphabetically sorted.

The specific CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

To pick an issue, simply add your name behind it. To learn more about how
this list is updated have a look at
https://wiki.debian.org/LTS/Development#Triage_new_security_issues

To make it easier to see the entire history of an update, please append notes
rather than remove/replace existing ones.

--
ansible
  NOTE: 20210411: As discussed with the maintainer I will update Buster first and
  NOTE: 20210411: after that LTS. (apo)
  NOTE: 20210426: https://people.debian.org/~apo/lts/ansible/
--
asterisk (Abhijith PA)
--
debian-archive-keyring (Anton)
  NOTE: https://lists.debian.org/debian-lts/2021/08/msg00037.html
  NOTE: 20210920: Raphael answered. will backport today. (utkarsh)
  NOTE: 20211003: waiting for Jonathan to get back as his keys
  NOTE: 20211003: seemed to have expired and the build is thus
  NOTE: 20211003: failing. Or at least appears to be. :( (utkarsh)
  NOTE: 20211018: Jonathan is prepping the branch; will work
  NOTE: 20211018: with him and upload and publish the DLA. (utkarsh)
--
expat (Emilio)
  NOTE: 20220221: please wait for DSA first. (Anton)
--
firmware-nonfree
  NOTE: 20210731: WIP: https://salsa.debian.org/lts-team/packages/firmware-nonfree
  NOTE: 20210828: Most CVEs are difficult to backport. Contacted Ben regarding possible "ignore" tag
  NOTE: 20211207: Intend to release this week.
--
freecad (Emilio)
  NOTE: 20220221: please wait for DSA first. (Anton)
--
gif2apng (Anton)
  NOTE: 20220114: orphaned package with inactive upstream, maybe coordinate with Debian QA to write our own patches (Beuc)
  NOTE: 20220114: CVEs unrelated to apng2gif's (Beuc)
  NOTE: 20220221: WIP (Anton)
--
gpac (Roberto C. Sánchez)
  NOTE: 20211101: coordinating with secteam for s-p-u since stretch/buster versions match (roberto)
  NOTE: 20211120: received OK from secteam for buster update, working on stretch/buster in parallel (roberto)
  NOTE: 20211228: Returning to active work on this now that llvm/rustc update is complete (roberto)
--
htmldoc (Thorsten Alteholz)
--
intel-microcode
  NOTE: 20220213: please recheck
--
libarchive (Thorsten Alteholz)
  NOTE: 20220213: testing package
--
libgit2 (Utkarsh)
  NOTE: 20220208: got clearance. will upload this week. (utkarsh)
  NOTE: 20220221: had been severely ill the past week. shall get it done soon. (utkarsh)
--
linux (Ben Hutchings)
--
linux-4.19 (Ben Hutchings)
--
mariadb-10.1
  NOTE: 20220222: Can be risky. Please consider backporting mariadb-10.3. See discussion https://lists.debian.org/debian-lts/2022/02/msg00005.html and coordinate with maintainer (Anton)
--
nvidia-graphics-drivers
   NOTE: 20220203: package is in non-free but also in packages-to-support (Beuc)
   NOTE: 20220209: monitor nvidia-graphics-drivers-legacy-390xx for a potential
   NOTE: 20220209: backport (apo)
--
pjproject (Abhijith PA)
  NOTE: 20211230: patch available for the no-dsa issue, check its NOTE (pochu)
  NOTE: 20220215: Asterisk and ring have embedded copy of pjproject (abhijith)
--
ring (Abhijith PA)
--
samba
  NOTE: 20211128: WIP https://salsa.debian.org/lts-team/packages/samba/
  NOTE: 20211212: Fix is too large, coordination with ELTS-upload (anton)
  NOTE: 20220110: fix applied, but will need a second opinion. (utkarsh)
  NOTE: 20220125: ftbfs, wip. (utkarsh)
--
thunderbird (Emilio)
--
tiff (Thorsten Alteholz)
--
ujson (Anton)
  NOTE: 20220121: please reheck, at least the mentioned function is available in Stretch
  NOTE: 20220206: https://salsa.debian.org/lts-team/packages/ujson Investigating, whether affected or not (Anton)
  NOTE: 20220221: WIP (Anton)
--
vim
--
